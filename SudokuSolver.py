from typing import *
import copy
import pygame
import time
from pygame import font

pygame.init()


class Board:
    grid = []
    gridbools = [[bool for i in range(9)] for i in range(9)]
    newgridbools = []

    def __init__(self, grid: List[List[int]]):
        self.grid = grid
        self.boolinit()

    def boolinit(self):
        for i in range(9):
            for j in range(9):
                if self.grid[i][j] != 0:
                    self.gridbools[i][j] = False
                else:
                    self.gridbools[i][j] = True
        self.newgridbools = copy.deepcopy(self.gridbools)

    def getLegal(self, x: int, y: int) -> List[int]:
        legal = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        ## checks which numbers row and column contain and removes them from the list of the legal numbers
        for i in range(9):
            for j in range(9):
                if i == x or j == y:
                    if self.grid[i][j] in legal:
                        legal.remove(self.grid[i][j])

        # ## checks which numbers are contained in the 3x3 square the cell is in and removes them from the list of legal numbers
        square = [x // 3, y // 3]
        for i in range(3):
            for j in range(3):
                if self.grid[square[0] * 3 + i][square[1] * 3 + j] in legal:
                    legal.remove(self.grid[square[0] * 3 + i][square[1] * 3 + j])

        if len(legal) == 0:
            return [0]
        return legal

    def __repr__(self):
        ret = ""
        for i in range(9):
            ret += "" + str(self.grid[i]) + "\n"
        return ret

    def advance(self, i, j):
        j += 1
        if j > 8:
            i += 1
            return i, 0
        return i, j

    def advance2(self, i, j):
        j -= 1
        if j < 0:
            i -= 1
            return i, 8
        return i, j

    def textonpos(self, screen, i, j):
        font = pygame.font.SysFont(None, 35)
        lightblue = [173, 216, 230]
        lightred = [255, 204, 203]
        lightgreen = [144, 238, 144]
        black = [0, 0, 0]
        sw, sh = 50, 50
        swo, sho = 10, 10
        tmpi = j * (sw + swo) + swo
        tmpj = i * (sh + sho) + sho
        tmpsurface = pygame.Surface((50, 50))

        if not self.newgridbools[i][j]:
            tmpsurface.fill(lightgreen)
        elif self.grid[i][j] == 0:
            tmpsurface.fill(lightred)
        else:
            tmpsurface.fill(lightblue)

        screentext = font.render(str(self.grid[i][j]), True, black)
        screen.blit(tmpsurface, (tmpi, tmpj))
        screen.blit(screentext, (tmpi + 17, tmpj + 19))

        pygame.display.update()

    def redraw(self, screen):
        for i in range(9):
            for j in range(9):
                self.textonpos(screen, i, j)

    def findFirstEmpty(self):
        for i in range(9):
            for j in range(9):
                if self.grid[i][j] == 0:
                    return i, j
        return 10, 10

    # without gui
    def solve(self):
        states = [(0, 0, copy.deepcopy(self.grid))]

        i = 0
        j = 0

        while i < 9 and j < 9:
            moved = False
            if self.gridbools[i][j]:

                legalMoves = self.getLegal(i, j)
                legalMoves.sort()

                for move in legalMoves:
                    if move > self.grid[i][j] and not moved:
                        self.grid[i][j] = move
                        states.append((i, j, copy.deepcopy(self.grid)))
                        i, j = self.advance(i, j)
                        moved = True
                        break
                if not moved:
                    i, j, self.grid = states.pop()
            else:
                i, j = self.advance(i, j)
        print("Done")
        print(self)

    # with gui normal
    def solve2(self):
        states = [(0, 0, copy.deepcopy(self.grid))]

        i = 0
        j = 0
        background_colour = (255, 255, 255)
        (width, height) = (550, 550)

        screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption('Sudoku Solver Visualizer')

        screen.fill(background_colour)
        pygame.display.flip()

        self.redraw(screen)

        pygame.display.update()

        while i < 9 and j < 9:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

            self.redraw(screen)
            moved = False
            if self.gridbools[i][j]:

                legalMoves = self.getLegal(i, j)
                legalMoves.sort()

                for move in legalMoves:
                    if move > self.grid[i][j] and not moved:
                        self.grid[i][j] = move
                        states.append((i, j, copy.deepcopy(self.grid)))
                        i, j = self.advance(i, j)
                        moved = True
                        break
                if not moved:
                    i, j, self.grid = states.pop()
            else:
                i, j = self.advance(i, j)

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

    # with gui backwards
    def solve3(self):
        states = [(0, 0, copy.deepcopy(self.grid))]

        i = 8
        j = 8
        background_colour = (255, 255, 255)
        (width, height) = (550, 550)

        screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption('Sudoku Solver Visualizer')

        screen.fill(background_colour)
        pygame.display.flip()

        self.redraw(screen)

        pygame.display.update()

        while i >= 0 and j >= 0:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

            moved = False
            if self.gridbools[i][j]:

                legalMoves = self.getLegal(i, j)
                legalMoves.sort()

                for move in legalMoves:
                    if move > self.grid[i][j] and not moved:
                        self.grid[i][j] = move
                        states.append((i, j, copy.deepcopy(self.grid)))
                        i, j = self.advance2(i, j)
                        moved = True
                        break
                if not moved:
                    i, j, self.grid = states.pop()
            else:
                i, j = self.advance2(i, j)
            self.redraw(screen)

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

    # with gui Checks if any positions are trivial (1 possible solve) and solves them
    # only checks trivials on startup
    def solve4(self):

        background_colour = (255, 255, 255)
        (width, height) = (550, 550)

        screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption('Sudoku Solver Visualizer')

        screen.fill(background_colour)
        pygame.display.flip()
        found1 = True

        while found1:
            found1 = False
            for k in range(9):
                for o in range(9):
                    if (self.gridbools[k][o]):
                        legals = self.getLegal(k, o)
                        print("{},{} {}".format(k, o, str(legals)))
                        if len(legals) == 1 and legals[0] != 0:
                            self.grid[k][o] = legals[0]
                            self.gridbools[k][o] = False
                            found1 = True
                            self.redraw(screen)

        states = [(0, 0, copy.deepcopy(self.grid))]

        pygame.display.update()

        i = 0
        j = 0

        while i < 9 and j < 9:

            moved = False
            if self.gridbools[i][j]:
                legalMoves = self.getLegal(i, j)
                legalMoves.sort()
                for move in legalMoves:
                    if move > self.grid[i][j] and not moved:
                        self.grid[i][j] = move
                        states.append((i, j, copy.deepcopy(self.grid)))
                        i, j = self.advance(i, j)
                        moved = True
                        break
                if not moved:
                    i, j, self.grid = states.pop()
            else:
                i, j = self.advance(i, j)
            self.redraw(screen)

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

    # with gui, Check if any positions are trivial every iteration
    def solve5(self):

        background_colour = (255, 255, 255)
        (width, height) = (550, 550)

        screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption('Sudoku Solver Visualizer')

        screen.fill(background_colour)
        pygame.display.flip()
        found1 = True

        while found1:
            found1 = False
            for k in range(9):
                for o in range(9):
                    if self.gridbools[k][o]:
                        legals = self.getLegal(k, o)
                        if len(legals) == 1 and legals[0] != 0:
                            self.grid[k][o] = legals[0]
                            self.gridbools[k][o] = False
                            found1 = True
                            self.redraw(screen)
        i, j = self.findFirstEmpty()

        states = [(i, j, copy.deepcopy(self.grid), copy.deepcopy(self.gridbools))]
        pygame.display.update()
        print("Solved first batch of trivials")
        while i < 9 and j < 9:
            moved = False
            if self.gridbools[i][j]:
                legalMoves = self.getLegal(i, j)
                legalMoves.sort()

                for move in legalMoves:
                    self.grid[i][j] = move
                    states.append((i, j, copy.deepcopy(self.grid), copy.deepcopy(self.gridbools)))
                    i, j = self.advance(i, j)
                    moved = True
                    break
                if not moved:
                    i, j, self.grid, self.gridbools = states.pop()
            else:
                i, j = self.advance(i, j)

            found1 = True

            while found1:
                found1 = False
                for k in range(9):
                    for o in range(9):
                        if self.gridbools[k][o] and self.grid[i][j] == 0:
                            legals = self.getLegal(k, o)
                            if len(legals) == 1 and legals[0] != 0:
                                self.grid[k][o] = legals[0]
                                self.gridbools[k][o] = False
                                found1 = True
                                self.redraw(screen)
            i, j = self.findFirstEmpty()
            print("redrawing")
            self.redraw(screen)

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False


# b = Board([
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 8, 0, 0, 0, 0]])

# b = Board([
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [6, 7, 2, 1, 9, 5, 3, 4, 8],
#     [1, 9, 8, 3, 4, 2, 5, 6, 7],
#     [8, 5, 9, 7, 6, 1, 4, 2, 3],
#     [4, 2, 6, 8, 5, 3, 7, 9, 1],
#     [7, 1, 3, 9, 2, 4, 8, 5, 6],
#     [9, 6, 1, 5, 3, 7, 2, 8, 4],
#     [2, 8, 7, 4, 1, 9, 6, 3, 5],
#     [3, 4, 5, 2, 8, 6, 1, 7, 9]])


# b = Board([
#     [0, 0, 0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 9, 5, 3, 0, 0],
#     [0, 9, 8, 0, 4, 2, 5, 0, 0],
#     [0, 5, 0, 7, 6, 1, 4, 0, 0],
#     [0, 2, 6, 0, 5, 3, 7, 0, 0],
#     [7, 1, 3, 0, 2, 4, 8, 0, 0],
#     [9, 6, 0, 0, 3, 7, 2, 0, 0],
#     [2, 0, 7, 4, 1, 9, 6, 0, 5],
#     [3, 0, 5, 0, 8, 0, 1, 0, 9]])



b = Board([
    [0, 0, 0, 7, 0, 0, 0, 0, 0],
    [1, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 4, 3, 0, 2, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 6],
    [0, 0, 0, 5, 0, 9, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 4, 1, 8],
    [0, 0, 0, 0, 8, 1, 0, 0, 0],
    [0, 0, 2, 0, 0, 0, 0, 5, 0],
    [0, 4, 0, 0, 8, 0, 3, 0, 0]])
b.solve2()
